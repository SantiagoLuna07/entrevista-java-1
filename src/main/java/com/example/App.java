package com.example;

import javax.naming.spi.DirStateFactory.Result;

import com.example.starwars.API;
import com.example.starwars.GetRequestRepository;
import com.example.starwars.Printer;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        API apiCalls = new API();
        GetRequestRepository repository = new GetRequestRepository(apiCalls);
        Printer printer = new Printer();

        // -------------------------------------------
        // Ejemplo: Imprimir los detalles de la pelicula "The Phantom Menace"
        //
//        JsonObject jsonObject = repository.getAll("films", "Phantom");
//        JsonArray filmresults = jsonObject.getAsJsonArray("results");
//        printer.printDetailsFilms(filmresults);

        // -------------------------------------------
        // Ejercicio 1: Imprimir en consola el Nombre del personaje, y el nombre de su especie, el cual haya aparecido en la mayor cantidad de peliculas.
        // Ejemplo:
        //
        //   Anakin Skywalker - Human
        //

        JsonObject jsonObj = repository.getAll("people", "");
        JsonArray peopleRs = jsonObj.getAsJsonArray("results");
        int maxFilms = 0;// Cantidad maxima de peliculas
        JsonObject pSelected = null;// Personaje seleccionado
        for (int i = 0; i < peopleRs.size(); i++) {
        	JsonObject people = peopleRs.get(i).getAsJsonObject();
        	JsonArray films = people.getAsJsonArray("films");        	

        	int count = 0;
        	for (;count < films.size();) {
        		count ++;
        	}

        	if (count > maxFilms) {
        		maxFilms = count;
        		pSelected = people;
        	}
        }

    	printer.printExercise1(pSelected);

        // -------------------------------------------
        // Ejercicio 2: Imprimir en consola una tabla resumen de todas las naves que ha piloteado "Obi-Wan Kenobi" en cada pelicula
        // Ejemplo:
        //
        //  Titulo Pelicula       | Nombre nave/vehiculo      | Tipo
        //  ----------------------|---------------------------|-------------------------
        //  Attack of the Clones  | Jedi starfighter          | starships
        //  Revenge of the Sith   | Trade Federation cruiser  | starships
        //  Revenge of the Sith   | Trade Federation cruiser  | starships
        //  The Phantom Menace    | Tribubble bongo           | vehicles
        //

    	JsonObject jsonObj2 = repository.getAll("people", "Obi-Wan%20Kenobi");
    	JsonArray characters = jsonObj2.getAsJsonArray("results");
    	JsonObject character = (JsonObject) characters.get(0);

    	printer.printExercise2(character);
        

        // -------------------------------------------
        // Ejercicio 3 (OPCIONAL): Modifique esta aplicacion para convertirla en una aplicacion web, la cual imprima una pagina HTML los resultados de los ejercicios anteriores.
        // Se recomienda utilizar String Framework (https://spring.io/guides/gs/spring-boot/)
        //


    }
}
